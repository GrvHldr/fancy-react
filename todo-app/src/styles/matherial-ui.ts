import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";

const useUIStyles = makeStyles((theme: Theme) => createStyles(
    {
        root: {
            width: '100%',
        },
        container: {
            maxHeight: 440,
        },
        button: {
            margin: theme.spacing(1),
        },
    }));

export default useUIStyles;
