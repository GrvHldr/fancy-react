import {combineReducers, createStore} from "redux";
import {FTodoReducer} from "./reducers";

const rootReducer = combineReducers({
    todoList: FTodoReducer
});

export type AppState = ReturnType<typeof rootReducer>;

export default function configureStore() {
    return createStore(rootReducer)
}
