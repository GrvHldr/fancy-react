import {ActionTypes, C_ADD_TODO, C_CHANGE_TODO, C_DELETE_TODO, C_TOGGLE_COMPLETED} from "./types";

export function FAddNewTodo(index: number, todo: string): ActionTypes {
    return {
        type: C_ADD_TODO,
        payload: {
            index,
            todo,
            completed: false
        }
    }
}

export function FUpdateTodo(index: number, todo: string): ActionTypes {
    return {
        type: C_CHANGE_TODO,
        payload: {
            index,
            todo,
        }
    }
}

export function FToggleTodo(index: number): ActionTypes {
    return {
        type: C_TOGGLE_COMPLETED,
        payload: {
            index
        }
    }
}

export function FDeleteTodo(index: number): ActionTypes {
    return {
        type: C_DELETE_TODO,
        payload: {
            index
        }
    }
}
