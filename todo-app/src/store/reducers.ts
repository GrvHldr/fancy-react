import {ActionTypes, C_ADD_TODO, C_CHANGE_TODO, C_DELETE_TODO, C_TOGGLE_COMPLETED, IToDoData} from "./types";

const cInitialState: IToDoData = {
    todoList: []
}


export function FTodoReducer(
    state = cInitialState,
    action: ActionTypes
): IToDoData {
    switch (action.type) {
        case C_ADD_TODO:
            return {
                todoList: [...state.todoList, action.payload]
            } as IToDoData;
        case C_CHANGE_TODO:
            return <IToDoData>{
                todoList: state.todoList.map((val) => (
                    val.index === action.payload.index ? {...val, todo: action.payload.todo} : val
                ))
            }
        case C_TOGGLE_COMPLETED:
            return {
                todoList: state.todoList.map(
                    (val) => val.index === action.payload.index ? {...val, completed: !val.completed} : val
                )
            }
        case C_DELETE_TODO:
            return {
                todoList: state.todoList.filter((val) => val.index !== action.payload.index)
            }
        default:
            return state
    }
}
