export interface IToDoDataItem {
    index: number
    todo: string
    completed: boolean
}

export interface IToDoData {
    todoList: IToDoDataItem[]
}

export const C_ADD_TODO = "ADD_TODO"
export const C_CHANGE_TODO = "CHANGE_TODO"
export const C_TOGGLE_COMPLETED = "TOGGLE_COMPLETED"
export const C_DELETE_TODO = "DELETE_TODO"


interface IAddNewTodo {
    type: typeof C_ADD_TODO
    payload: IToDoDataItem
}

interface IChangeTodo {
    type: typeof C_CHANGE_TODO
    payload: {
        index: number
        todo: string
    }
}

interface IToggleTodoCompleted {
    type: typeof C_TOGGLE_COMPLETED
    payload: {
        index: number
    }
}

interface IDeleteTodo {
    type: typeof C_DELETE_TODO
    payload: {
        index: number
    }
}

export type ActionTypes = IChangeTodo | IAddNewTodo | IToggleTodoCompleted | IDeleteTodo;
