import React from 'react';
import './styles/app.css';
import {ViewEditComponent} from "./components/ViewEditComponent";
import {IToDoData} from "./store/types";
import {FAddNewTodo, FDeleteTodo, FToggleTodo, FUpdateTodo} from "./store/actions";
import {connect} from "react-redux";
import {AppState} from "./store";
import {GroupComponent} from "./components/GroupComponent";

interface AppProps {
    todoList: IToDoData
    addNewTodo: typeof FAddNewTodo
    deleteTodo: typeof FDeleteTodo
    toggleTodo: typeof FToggleTodo
    updateTodo: typeof FUpdateTodo
}

interface iAppState {
    lastIndex: number
}

class App extends React.Component<AppProps, iAppState> {
    state = {
        lastIndex: 1
    }

    constructor(props: AppProps) {
        super(props);
        this.updateTodo = this.updateTodo.bind(this);
    }

    addNewTodo = (todo: string) => {
        this.props.addNewTodo(this.state.lastIndex, todo)
        this.setState((prev) => ({...prev, lastIndex: prev.lastIndex + 1}))
    }

    deleteTodo = (index: number) => {
        this.props.deleteTodo(index)
    }

    toggleTodo = (index: number) => {
        this.props.toggleTodo(index)
    }

    updateTodo(index: number, todo: string) {
        this.props.updateTodo(index, todo);
    }

    render() {
        return (
            <div className="App">
                <header className="App-header">TODO-app Demo</header>
                <div className="Container">
                    <div className="Nav-left">
                        <GroupComponent items={this.props.todoList.todoList} label={"ALL TODOS"}/>
                    </div>
                    <div className="Nav-center">
                        <ViewEditComponent
                            label={"TEST TEST TEST"}
                            data={this.props.todoList}
                            addNewTodo={this.addNewTodo}
                            deleteTodo={this.deleteTodo}
                            toggleTodo={this.toggleTodo}
                            updateTodo={this.updateTodo}
                        />
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state: AppState) => ({
    todoList: state.todoList,
});

export default connect(
    mapStateToProps,
    {
        addNewTodo: FAddNewTodo,
        deleteTodo: FDeleteTodo,
        toggleTodo: FToggleTodo,
        updateTodo: FUpdateTodo,
    }
)(App)
