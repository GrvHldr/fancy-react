import React from "react";
import {IToDoData, IToDoDataItem} from "../store/types"
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import TableContainer from '@material-ui/core/TableContainer';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import {TableBody} from "@material-ui/core";
import Switch from '@material-ui/core/Switch';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import TextField from '@material-ui/core/TextField';
import TablePagination from '@material-ui/core/TablePagination';
import useUIStyles from "../styles/matherial-ui";
import {NewActionComponent} from "./NewTodoActionComponent";


interface iViewEditProps {
    label: string
    data: IToDoData
    addNewTodo: (todo: string) => void
    deleteTodo: (index: number) => void
    toggleTodo: (index: number) => void
    updateTodo: (index: number, todo: string) => void
}

interface iViewEditTableProps {
    todoList: IToDoDataItem[]
    addNewTodo: (todo: string) => void
    deleteTodo: (index: number) => void
    toggleTodo: (index: number) => void
    updateTodo: (index: number, todo: string) => void
}

interface iCustomDeleteElemProps {
    deleteItem: () => void
}

interface iColumn {
    id: 'index' | 'completed' | 'todo' | 'trash';
    label: string;
    minWidth?: number;
    align?: 'right' | 'center' | 'left';
    format?: (value: number) => string;
}

interface iCustomTableCellProps {
    col: iColumn
    item: IToDoDataItem
    deleteItem: (index: number) => void
    toggleTodo: (index: number) => void
    updateTodo: (index: number, todo: string) => void
}

interface iCustomTableRowProps {
    item: IToDoDataItem
    deleteItem: (index: number) => void
    toggleTodo: (index: number) => void
    updateTodo: (index: number, todo: string) => void
}

interface iCustomSwitchProps {
    on: boolean
    toggle: () => void
}

const cColumns: iColumn[] = [
    {id: "index", label: "ID", minWidth: 50, align: "center"},
    {id: "completed", label: "Task Done", minWidth: 50, align: "center"},
    {id: "todo", label: "Description", minWidth: 200, align: "left"},
    {id: "trash", label: "Delete", minWidth: 50, align: "center"}
]

const CustomSwitch: React.FunctionComponent<iCustomSwitchProps> = ({on, toggle}) => {
    return (
        <Switch
            checked={on}
            onChange={toggle}
            inputProps={{'aria-label': 'primary checkbox'}}
        />
    )
}

const CustomDeleteElement: React.FunctionComponent<iCustomDeleteElemProps> = ({deleteItem}) => {
    return (
        <IconButton edge="end" aria-label="delete" onClick={deleteItem}>
            <DeleteIcon/>
        </IconButton>
    )
}

const CustomTableCell: React.FunctionComponent<iCustomTableCellProps> = ({
                                                                             col,
                                                                             item,
                                                                             deleteItem,
                                                                             toggleTodo,
                                                                             updateTodo
                                                                         }) => {
    switch (col.id) {
        case "index":
            return (
                <TableCell align={col.align}>
                    <Box color="primary.main">{item.index}</Box>
                </TableCell>
            )
        case "completed":
            return (
                <TableCell align={col.align}>
                    <CustomSwitch
                        on={item.completed}
                        toggle={() => toggleTodo(item.index)}
                        key={col.id}
                    />
                </TableCell>
            )
        case "todo":
            return (
                <TableCell align={col.align}>
                    <TextField
                        label={"put todo action here"}
                        defaultValue={item.todo}
                        onChange={(e) => {
                            updateTodo(item.index, e.target.value);
                        }}
                        style={{width: "100%"}}
                    />
                </TableCell>
            )
        case "trash":
            return (
                <TableCell align={col.align}>
                    <CustomDeleteElement deleteItem={() => deleteItem(item.index)}/>
                </TableCell>
            )
    }
}

const CustomTableRow: React.FunctionComponent<iCustomTableRowProps> = ({
                                                                           item,
                                                                           deleteItem,
                                                                           toggleTodo,
                                                                           updateTodo
                                                                       }) => {
    return (
        <TableRow hover role={"checkbox"} tabIndex={-1}>
            {cColumns.map((col) => (
                <CustomTableCell
                    col={col}
                    item={item}
                    deleteItem={deleteItem}
                    toggleTodo={toggleTodo}
                    updateTodo={updateTodo}
                    key={`${item.index}${col.id}`}
                />
            ))}
        </TableRow>
    )
}

const ViewEditTable: React.FunctionComponent<iViewEditTableProps> = ({
                                                                         todoList,
                                                                         addNewTodo,
                                                                         deleteTodo,
                                                                         toggleTodo,
                                                                         updateTodo
                                                                     }) => {
    const classes = useUIStyles();
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);

    const handleChangePage = (event: unknown, newPage: number) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    return (
        <Paper className={classes.root}>
            <TableContainer className={classes.container}>
                <Table stickyHeader aria-label="sticky table">
                    <TableHead>
                        {cColumns.map((col) => (
                            <TableCell
                                key={col.id}
                                align={col.align}
                                style={{minWidth: col.minWidth}}
                            >
                                {col.label}
                            </TableCell>
                        ))}
                    </TableHead>
                    <TableBody>
                        {todoList.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => (
                            <CustomTableRow
                                item={row}
                                deleteItem={deleteTodo}
                                toggleTodo={toggleTodo}
                                updateTodo={updateTodo}
                                key={row.index}
                            />
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            <TablePagination
                rowsPerPageOptions={[5, 10, 15]}
                component="div"
                count={todoList.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
            />
            <br/>
            <NewActionComponent
                sendTodoAction={addNewTodo}
            />
        </Paper>
    )
}

export class ViewEditComponent extends React.Component<iViewEditProps> {

    render() {
        return (
            <Typography component={"div"}>
                <Box color="primary.main">{this.props.label}</Box>
                <ViewEditTable
                    todoList={this.props.data.todoList}
                    addNewTodo={this.props.addNewTodo}
                    deleteTodo={this.props.deleteTodo}
                    toggleTodo={this.props.toggleTodo}
                    updateTodo={this.props.updateTodo}
                />
            </Typography>
        );
    }
}
