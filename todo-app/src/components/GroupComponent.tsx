import React from "react";

import {Accordion, AccordionSummary, Typography} from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import {IToDoDataItem} from "../store/types";


export interface IGroupComponentProps {
    items: IToDoDataItem[]
    label: string
}


export class GroupComponent extends React.Component<IGroupComponentProps> {
    private readonly divRef: React.RefObject<any>;

    constructor(props: IGroupComponentProps) {
        super(props);

        this.divRef = React.createRef();
    }

    render() {
        return (
            <div ref={this.divRef}>
                {
                    <Accordion key={this.props.label} defaultExpanded={true}>
                        <AccordionSummary expandIcon={<ExpandMoreIcon/>}>
                            <Typography variant="h6" noWrap>
                                {this.props.label}
                            </Typography>
                        </AccordionSummary>
                        <ul>
                            {this.props.items.map((val) => (
                                <li key={val.index} style={val.completed? {textDecoration: "line-through"} : {}}>
                                    {val.index}. {val.todo}
                                </li>
                            ))}
                        </ul>
                    </Accordion>
                }
            </div>
        )
    }
}
