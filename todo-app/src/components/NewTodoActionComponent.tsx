import * as React from "react"
import useUIStyles from "../styles/matherial-ui";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Icon from "@material-ui/core/Icon";

interface iTodoActionComponentProps {
    sendTodoAction: (action: string) => void
}

export const NewActionComponent: React.FunctionComponent<iTodoActionComponentProps> = ({
    sendTodoAction
}) => {
    const classes = useUIStyles();
    const [newTodo, setNewTodo] = React.useState("");

    const onButtonClick = () => {
        sendTodoAction(newTodo);
        setNewTodo("");
    }

    const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setNewTodo(event.target.value);
    }

    return (
        <form className={classes.root} noValidate autoComplete="off">
            <div>
                <TextField
                    required
                    label={"New TODO action item"}
                    style={{width: "50%"}}
                    value={newTodo}
                    onChange={onChange}
                />
                <Button
                    className={classes.button}
                    variant="contained"
                    color="primary"
                    endIcon={<Icon>send</Icon>}
                    onClick={onButtonClick}
                    disabled={!newTodo}
                > ADD </Button>
            </div>
        </form>
    )
}
